#!/usr/bin/env python

from setuptools import setup, find_packages

version = '1.3.2-dev'

setup(
    name='org.terranova.site',
    version=version,
    description="CCAIH Policy Product",
    # long_description=open("README.txt").read() + "\n" +
    #                  open(os.path.join("docs", "HISTORY.txt")).read(),
    # Get more strings from
    # http://pypi.python.org/pypi?:action=list_classifiers
    classifiers=[
        "Framework :: Plone",
        "Programming Language :: Python",
    ],
    keywords='',
    author='',
    author_email='',
    url='http://svn.plone.org/svn/collective/',
    license='GPL',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    namespace_packages=['org', 'org.terranova'],
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'setuptools',  # distribute
        'org.terranova.api',
        'org.terranova.data',
        'org.terranova.diazo',
        'Products.AutoUserMakerPASPlugin',
        'Products.ShibbolethPermissions',
        'sc.social.like',
        'gu.repository.content',
        'collective.js.jqueryui',
        'collective.js.uix.multiselect',
        'collective.geo.contentlocations',
        'collective.geo.geographer',
        'collective.geo.kml',
        'collective.geo.mapwidget',
        'collective.geo.openlayers',
        'collective.geo.settings',
        'collective.z3cform.mapwidget',
        'collective.googleanalytics',
        'collective.quickupload',
        'collective.onlogin',
        'collective.z3cform.widgets',
        'gu.plone.rdf',
        'plone.app.folderui',
        'plone.app.relationfield',
        'plone.formwidget.contenttree',
        'plone.formwidget.autocomplete',
        'collective.googleanalytics',
        'dexterity.membrane',
        'borg.localrole',
        'rdflib-sparql',
        "Products.ContentWellPortlets",
        "five.grok"
    ],
    extras_require={
        'test': ['plone.app.testing']
    },

    entry_points="""
    # -*- Entry points: -*-
    [z3c.autoinclude.plugin]
    target = plone
    """,
)
