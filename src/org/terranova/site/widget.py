from plone.formwidget.autocomplete.widget import AutocompleteMultiSelectionWidget

import z3c.form.interfaces
from z3c.form.browser.select import SelectFieldWidget

import zope.component
import zope.interface
import zope.schema.interfaces
from zope.interface import implementer


@zope.component.adapter(zope.schema.interfaces.IUnorderedCollection, z3c.form.interfaces.IFormLayer)
@zope.interface.implementer(z3c.form.interfaces.IFieldWidget)
def SimpleMultiSelectFieldWidget(field, request):
    """IFieldWidget factory for SelectWidget."""

    widget = SelectFieldWidget(field, field.value_type, request)
    widget.size = 5
    widget.multiple = 'multiple'
    widget.style = u'width:500px;'  # TODO: do this in css
    widget.addClass(u'uixmultiselect')

    return widget


@implementer(z3c.form.interfaces.IFieldWidget)
def AutocompleteMultiFieldWidget(field, request):
    widget = AutocompleteMultiSelectionWidget(request)
    widget.maxResults = 1000
    return z3c.form.widget.FieldWidget(field, widget)
