
(function($) {  

  $(document).ready(function()
  {
      $sel = $('select.uixmultiselect');
      if ($sel.is(':hidden')) {
          // select is hidden we have to refresh it later.
          tabs = $('select.formTabs, ul.formTabs');
            tabs.bind("onClick", function (e, index) {
                var curpanel = $(this).data('tabs').getCurrentPane();
                curpanel.find('select.uixmultiselect').multiselect(); // refresh
            });
      }
      $sel.filter(':visible').multiselect();
  });

})(jQuery); 

