# Module:   vocabs
# Date:     12th September 2013
# Author:   James Mills, j dot mills at griffith dot edu dot au

"""RDF Vocabularies"""


from rdflib import Namespace


ANDS = Namespace(u"http://purl.org/ands/ontologies/vivo/")
CVOCAB = Namespace(u"http://namespaces.griffith.edu.au/collection_vocab#")
BIBO = Namespace(u"http://purl.org/ontology/bibo/")
VIVO = Namespace(u"http://vivoweb.org/ontology/core#")
DCTERMS = Namespace(u"http://purl.org/dc/terms/")
