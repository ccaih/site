import logging
from Products.CMFCore.utils import getToolByName 

PROFILE_ID='profile-org.terranova.site:default'

def upgrade_1000_to_1001(context, logger=None):

    if logger is None:
        # Called as upgrade step: define our own logger.
        logger = logging.getLogger('org.terranova.site')

    setup = getToolByName(context, 'portal_setup')
    setup.runAllImportStepsFromProfile(PROFILE_ID)

    # qi = getToolByName(context, 'portal_quickinstaller')
    # qi.reinstallProducts([....])
        
