#!/usr/bin/env python


from json import loads
from re import compile as compile_regex


REGIONS = {
    "IBRA": "regions_ibra.json",
    "LGA": "regions_lga.json",
    "NRM": "regions_nrm.json",
}

TEMPLATE = """:{} a :{} ;
    rdfs:label "{}" ;
    ."""


PATTERN = compile_regex("\W+")


def name2id(name):
    return PATTERN.sub("", name)


with open("terranova_regions.ttl.in", "r") as inf:
    with open("terranova_regions.ttl", "w") as outf:
        outf.write(inf.read())

        for region, source in REGIONS.items():
            names = loads(open(source, "r").read())
            outf.write("\n\n".join((TEMPLATE.format(name2id(name), "{}Region".format(region), name) for name in names)))
        outf.write("\n\n")
