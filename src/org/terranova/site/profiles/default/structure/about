id: about
title: About
language: en
effectiveDate: 2012/12/10 21:49:9.749204 US/Eastern
creation_date: 2012/12/10 19:16:38.981661 US/Eastern
modification_date: 2012/12/10 21:49:9.749825 US/Eastern
creators: admin
allowDiscussion: 0
excludeFromNav: 0
presentation: 0
tableContents: 0
Content-Type: text/html

<div class="panel-background-gray" id="searchheader">
<h4>About</h4>
</div>
<div class="row-fluid whitebg">
<div class="container bs-docs-example">
<div class="row-fluid">
<div id="itemtext">
<div id="itemtitle"></div>
<div class="content">
<p> </p>
<p>Climate change adaptation consists of actions undertaken to reduce the adverse consequences of climate change, as well as to harness any beneficial opportunities that may arise from those actions. Adaptation actions aim to reduce the impacts of climate stresses on human and natural systems. The actual actions society take are best  understood as “responses” that can take the form of, among other things, (i) new public policy including regulatory frameworks, (ii) new design and engineering standards and guidelines, and (iii) new planning protocols and principles.</p>
<p>However, it is critical to appreciate that these potential societal responses emerge from a more comprehensive adaptation assessment research framework. As illustrated, key elements of climate change adaptation research include monitoring the impact of climate change on human and natural systems, quantifying system vulnerability typically in a risk assessment context, and monitoring the efficacy of societal responses.</p>
<p><strong>Purpose Of The Project</strong><br /> This project addresses research infrastructure needs for investigations into climate change adaptation research. We are now locked into a significant degree of climate change and there is an imperative for new knowledge, approaches and technologies to enable society to adapt to the unavoidable impacts of climate change. However, climate change is a new social problem and thus climate change adaptation (<a href="#definition" id="popout">click for definition</a>) is an emerging field of inquiry. Furthermore, the adaptation research agenda is becoming both deeper and broader.</p>
<p>All sectors in society (government, private, civil society) need climate change adaptation responses on multiple scales. Responses will include new national policy, revised state planning guidelines, ecosystem-based approaches, and technological innovation. However, implementation of climate change adaptation responses is inevitably “local”. Regional and local authorities undertake many of the responses, involving private sector innovation. Capturing data and information from these diverse sources is a fundamental challenge for researchers. Therefore, the Hub needs to focus on data and tools that reach down to the local scale and are relevant to local governments and regional planning authorities. It will provide an integrated hub for information (including adaptation data and tools) emerging from, and used by, all components of the climate change assessment research framework.</p>
<p><strong>What This Project Will Deliver<br /> </strong>This project aims to build a software system that acts as a central information hub for researchers in the Climate Change Adaptation research domain.The system will allow users to deposit research data, with associated metadata descriptions, into a central managed storage infrastructure. The system will also enable a variety of search types (including spatial and temporal searching of metadata) in addition to other discovery tools to locate relevant research data stored in the information hub.</p>
<p>Specifically, the project activities will include the design, engineering, testing and delivering of a research information hub. This will be a web-based service for identifying, assimilating, and making available, climate change adaptation information (including data and associated tools) to assist researchers in undertaking climate change adaptation research, as well as preserving research data from research.</p>
<p><strong>The Partners<br /> </strong>The project is a joint initiative between Climate Change Adaptation Response Program, Griffith School of the Environment and eResearch Services, in the Division of Information Services at Griffith University (<a href="www.griffith.edu.au/research/research-excellence/griffith-climate-change-response-program" target="_blank" title="Griffith Climate Change Response Program">www.griffith.edu.au/research/research-excellence/griffith-climate-change-response-program</a>), the Queensland CyberInfrastructure Foundation (QCIF) (<a href="http://www.qcif.edu.au/" target="_blank" title="Queensland Cyber Infrastructure Foundation">www.qcif.edu.au</a>), and the Australian National Data Service (<a href="http://www.ands.org.au/" target="_blank" title="Australian National Data Service">www.ands.org.au</a>).</p>
<p>Many other organisations will be involved in the development of the solution. These will be listed on the project website coming shortly.</p>
</div>
<p> </p>
<hr /></div>
</div>
</div>
<!--/row--></div>
