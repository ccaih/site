# Module:   utilitites
# Date:     24th Map 2013
# Author:   Gerhard Weis, g dot weis at griffith dot edu dot au


"""Site Package Utilities

For now this contains two utility classes to synchronize metadata
between Plone and a Triple Store (e.g: 4store)
"""


# 3rd-party Library Imports
from ordf.namespace import DCES, FOAF

from rdflib import Literal, OWL, RDF, RDFS, URIRef


# Zope Library Imports
from zope.interface import implements, implementer


# Our Imports
from gu.z3cform.rdf.interfaces import IRDFTypeMapper

from gu.plone.rdf.interfaces import IRDFContentTransform
from gu.repository.content.interfaces import (
    IRepositoryContainer,
    IRepositoryItem
)


# Local Imports
from .content.user import ITerranovaUser
from .content.group import ITerranovaGroup
from .vocabs import ANDS, BIBO, CVOCAB, DCTERMS


class RDFContentMapper(object):

    implements(IRDFContentTransform)

    def tordf(self, content, graph):
        if IRepositoryItem.providedBy(content):
            graph.add((graph.identifier, RDF['type'], CVOCAB['Item']))
        elif IRepositoryContainer.providedBy(content):
            graph.add((graph.identifier, RDF['type'], CVOCAB['Collection']))
        elif ITerranovaUser.providedBy(content):
            graph.add((graph.identifier, RDF['type'], FOAF['Person']))
        elif ITerranovaGroup.providedBy(content):
            graph.add(
                (
                    graph.identifier,
                    RDF['type'],
                    FOAF['Group']
                )
            )  # foaf:Organization

        graph.add((graph.identifier, RDF['type'], OWL['Thing']))


class PloneToRDFSyncer(object):

    implements(IRDFContentTransform)

    def tordf(self, content, graph):
        # FIXME: use only one way to describe things ....
        #        see dc - RDF mapping at
        #        http://dublincore.org/documents/dcq-rdf-xml/
        #        maybe dc app profile not as good as it might sound,
        #        but translated to RDF is better (or even owl)
        # FIXME: maybe move the next part into a separate utility
        # TODO: check content for attributes/interface before trying
        #        to access them

        if content.Type() == "User":
            fullname = "{0:s} {1:s}".format(
                content.first_name,
                content.last_name
            )

            properties = (
                (DCTERMS['title'],  Literal(fullname)),
                (RDFS['label'],     Literal(fullname)),
            )
        else:
            properties = (
                (
                    DCTERMS['title'],
                    Literal(content.title)
                ),
                (
                    RDFS['label'],
                    Literal(content.title)
                ),
                (
                    RDFS['comment'],
                    Literal(content.description)
                )
            )

            if getattr(content, "body", None) is not None:
                properties += (
                    (DCTERMS['description'], Literal(content.body.raw)),
                )

            if getattr(content, "description", None) is not None:
                properties += (
                    (BIBO["shortDescription"],  Literal(content.description)),
                )

        properties += (
            (
                DCES["identifier"],
                URIRef(content.absolute_url()),
            ),
            (
                ANDS["hasAssociationWith"],
                Literal("http://nla.gov.au/nla.party-465942"),
            ),
            (
                ANDS["makesAvailable"],
                Literal("https://www.terranova.org.au/terranova"),  # noqa
            ),
        )

        for prop, val in properties:
            if graph.value(graph.identifier, prop) is None:
                graph.add((graph.identifier, prop, val))
            elif graph.value(graph.identifier, prop) != val:
                graph.set((graph.identifier, prop, val))


@implementer(IRDFTypeMapper)
class RDFTypeMapper(object):

    def __init__(self, context, request, form):
        self.context = context
        self.request = request
        self.form = form

    def applyTypes(self, graph):
        pt = self.form.portal_type
        typemap = {
            'org.terranova.content.user': FOAF['Person'],
            'org.terranova.content.group': FOAF['Group'],
            'gu.repository.content.RepositoryItem': CVOCAB['Item'],
            'gu.repository.content.RepositoryContainer': CVOCAB['Collection'],
            'File': CVOCAB['File']
        }

        rdftype = typemap.get(pt, OWL['Thing'])
        graph.add((graph.identifier, RDF['type'], rdftype))
