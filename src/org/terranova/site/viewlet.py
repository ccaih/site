from gu.repository.content.interfaces import IRepositoryItem
from plone.app.layout.viewlets import common as base
from plone.app.layout.navigation.defaultpage import isDefaultPage
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile

class ListingViewlet(base.ViewletBase):

    index = ViewPageTemplateFile('listingviewlet.pt')

    @property
    def folder(self):
        return self.context.__parent__

    def isEnabled(self):
        """
        @return: Should this viewlet be rendered on this page.
        """
        # Some logic based self.context here whether Javascript should be included on this page or not
        # render only if context is defaultpage of IRepositoryItem
        if (IRepositoryItem.providedBy(self.context.__parent__) and
            isDefaultPage(self.context.__parent__, self.context)):
            return True
        return False

    def render(self):
        """ Render viewlet only if it is enabled.

        """

        # Perform some condition check
        if self.isEnabled():
            # Call parent method which performs the actual rendering
            return super(ListingViewlet, self).render()
        else:
            # No output when the viewlet is disabled
            return ""
