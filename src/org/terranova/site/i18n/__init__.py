#!/usr/bin/env python
# -*- coding: utf8 -*-

# Module:   i18n
# Date:     29th August 2013
# Author:   James Mills, j dot mills at griffith dot edu dot au

"""i18n support"""

from zope.i18nmessageid import MessageFactory


_ = MessageFactory(__package__.split(".")[1])  # org.<name>.site.i18n
