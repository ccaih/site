#!/usr/bin/env python
# -*- coding: utf8 -*-

# Module:   container
# Date:     24th September 2013
# Author:   James Mills, j dot mills at griffith dot edu dot au

"""Custom Browser Views for Repository Containers"""


from contextlib import contextmanager


from plone.dexterity.browser.edit import DefaultEditForm
from plone.dexterity.browser.add import DefaultAddForm, DefaultAddView


from org.terranova.site.i18n import _


FIELDMODS = {
    "IDublinCore.subjects": {
        "title": _(u"Thematic Tags"),
        "description": _(u"Enter one or more keywords that describes subject matter of the content item.  Type ahead to choose an existing keyword or create a new keyword."),
    },
}


@contextmanager
def field(fields, name):
    field = fields[name]
    yield field.field


@contextmanager
def widget(widgets, name):
    yield widgets[name]


class AddForm(DefaultAddForm):

    def deleteField(self, name):
        if name in self.fields:
            del self.fields[name]
        else:
            for group in self.groups:
                if name in group.fields:
                    del group.fields[name]
                    return

    def updateFields(self):
        super(AddForm, self).updateFields()

        # Collect all fields in all groups and on this form into a mapping for easy access.
        fields = dict((k, v) for group in (group.fields.items() for group in self.groups) for k, v in group)
        fields.update(self.fields.items())

        # Update field titles and descriptions
        for k, mods in FIELDMODS.items():
            with field(fields, k) as f:
                for a, v in mods.items():
                    setattr(f, a, v)

        # Delete unwanted fields
        self.deleteField("IDublinCore.contributors")
        self.deleteField("IDublinCore.creators")
        self.deleteField("IDublinCore.language")
        self.deleteField("IDublinCore.rights")


class AddView(DefaultAddView):

    form = AddForm

    def label(self):
        return _(u"Collection")


class EditForm(DefaultEditForm):

    def label(self):
        return _(u"Edit Collection")

    def deleteField(self, name):
        if name in self.fields:
            del self.fields[name]
        else:
            for group in self.groups:
                if name in group.fields:
                    del group.fields[name]
                    return

    def updateFields(self):
        super(EditForm, self).updateFields()

        # Collect all fields in all groups and on this form into a mapping for easy access.
        fields = dict((k, v) for group in (group.fields.items() for group in self.groups) for k, v in group)
        fields.update(self.fields.items())

        # Update field titles and descriptions
        for k, mods in FIELDMODS.items():
            with field(fields, k) as f:
                for a, v in mods.items():
                    setattr(f, a, v)

        # Delete unwanted fields
        self.deleteField("IDublinCore.contributors")
        self.deleteField("IDublinCore.creators")
        self.deleteField("IDublinCore.language")
        self.deleteField("IDublinCore.rights")
