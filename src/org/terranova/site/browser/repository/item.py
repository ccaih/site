#!/usr/bin/env python
# -*- coding: utf8 -*-

# Module:   item
# Date:     2th August 2013
# Author:   James Mills, j dot mills at griffith dot edu dot au

"""Custom Browser Views for Repository Items"""


from contextlib import contextmanager


from Products.CMFCore.utils import getToolByName
from plone.dexterity.browser.add import DefaultAddForm, DefaultAddView
from plone.dexterity.browser.edit import DefaultEditForm

from z3c.form import validator
from z3c.form.field import Fields

from zope.schema import Bool
from zope.interface import Invalid, Interface
from zope.browserpage import ViewPageTemplateFile


from org.terranova.site.i18n import _


FIELDMODS = {
    "body": {
        "title": _(u"Detailed Summary"),
        "description": _(
            u"Please provide an detailed overview of the content being uploaded. "
            u"This is the equivalent of an abstract or short executive summary. No more than 300 words."
        ),
    },
    "logo": {
        "description": _(u"This is the logo of the owning organisation."),
    },
    "IDublinCore.title": {
        "description": _(u"This is the name of your report or dataset and will be the primary description that shows up in search results."),
    },
    "IDublinCore.expires": {
        "title": _(u"Expiration Date"),
        "description.expires": _(u"Please enter the date that you want your content item to STOP show up in listings and searches on TerraNova."),
    },
    "IDublinCore.subjects": {
        "title": _(u"Thematic Tags"),
        "description": _(
            u"Enter one or more keywords that describes subject matter of the content item.  Type ahead to choose an existing keyword or create a new keyword."
            u"You are able to choose your own keywords."
        ),
    },
    "IDublinCore.effective": {
        "title": _(u"Upload Date"),
        "description": _(
            u"Please enter the date that you want your content item to show up in listings and searches on Terra Nova. "
            u"This could help to accommodate for an embargo period on the content item."
        ),
    },
    "IDublinCore.description": {
        "title": _(u"Brief Description"),
        "description.description":  _(
            u"The key points about the content being uploaded. It's purpose, focus and any outcomes."
            u"Bullet points are preferred and no more than 100 words."
        ),
    },
}


@contextmanager
def field(fields, name):
    field = fields[name]
    yield field.field


@contextmanager
def widget(widgets, name):
    yield widgets[name]


class IAccept(Interface):

    accept = Bool(
        title=_(u"I have read and understood the "),
        description=_(u"Please read and indicate that you agree to the Statement of Expectations by checking the checkbox above."),
        required=True,
        default=False
    )


class AcceptAdapter(object):
    """IAccept Adapter"""

    def __init__(self, *args, **kwargs):
        super(AcceptAdapter, self).__init__()


class AddForm(DefaultAddForm):

    def deleteField(self, name):
        if name in self.fields:
            del self.fields[name]
        else:
            for group in self.groups:
                if name in group.fields:
                    del group.fields[name]
                    return

    def updateFields(self):
        super(AddForm, self).updateFields()

        self.fields += Fields(IAccept, ignoreContext=True)

        # Collect all fields in all groups and on this form into a mapping for easy access.
        fields = dict((k, v) for group in (group.fields.items() for group in self.groups) for k, v in group)
        fields.update(self.fields.items())

        # Update field titles and descriptions
        for k, mods in FIELDMODS.items():
            with field(fields, k) as f:
                for a, v in mods.items():
                    setattr(f, a, v)

        # Delete unwanted fields
        self.deleteField("IDublinCore.contributors")
        self.deleteField("IDublinCore.creators")
        self.deleteField("IDublinCore.language")
        self.deleteField("IDublinCore.rights")

    def updateWidgets(self):
        super(AddForm, self).updateWidgets()

        portal_url = getToolByName(self.context, "portal_url")
        site = portal_url.getPortalObject()

        # Update accept widget
        with widget(self.widgets, "accept") as accept:
            accept.title = _(u"Statement of Expectations")
            accept.link = {"title": _(u"Statement of Expectations"), "url": site.expectations.absolute_url()}
            accept.template = ViewPageTemplateFile("templates/accept.pt")

        # Update title on Save button (Issue #49)
        self.buttons.get("save").title = u"Save and Exit"


class AcceptValidator(validator.SimpleFieldValidator):

    def validate(self, value):
        if not value:
            raise Invalid(u"You must accept the terms and conditions!")


validator.WidgetValidatorDiscriminators(AcceptValidator, field=IAccept["accept"])


class AddView(DefaultAddView):

    form = AddForm

    def label(self):
        return _(u"Content")


class EditForm(DefaultEditForm):

    def label(self):
        return _(u"Edit Content")

    def deleteField(self, name):
        if name in self.fields:
            del self.fields[name]
        else:
            for group in self.groups:
                if name in group.fields:
                    del group.fields[name]
                    return

    def updateFields(self):
        super(EditForm, self).updateFields()

        self.fields += Fields(IAccept, ignoreContext=True)

        # Collect all fields in all groups and on this form into a mapping for easy access.
        fields = dict((k, v) for group in (group.fields.items() for group in self.groups) for k, v in group)
        fields.update(self.fields.items())

        # Update field titles and descriptions
        for k, mods in FIELDMODS.items():
            with field(fields, k) as f:
                for a, v in mods.items():
                    setattr(f, a, v)

        # Delete unwanted fields
        self.deleteField("IDublinCore.contributors")
        self.deleteField("IDublinCore.creators")
        self.deleteField("IDublinCore.language")
        self.deleteField("IDublinCore.rights")

    def updateWidgets(self):
        super(EditForm, self).updateWidgets()

        portal_url = getToolByName(self.context, "portal_url")
        site = portal_url.getPortalObject()

        # Update accept widget
        with widget(self.widgets, "accept") as accept:
            accept.title = _(u"Statement of Expectations")
            accept.link = {"title": _(u"Statement of Expectations"), "url": site.expectations.absolute_url()}
            accept.template = ViewPageTemplateFile("templates/accept.pt")

        # Update title on Save button (Issue #49)
        self.buttons.get("save").title = u"Save and Exit"
