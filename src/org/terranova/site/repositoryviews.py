from Products.Five.browser import BrowserView
from plone.app.uuid.utils import uuidToCatalogBrain
from gu.repository.content.interfaces import IRepositoryMetadata
from ordf.namespace import DC
from gu.z3cform.rdf.interfaces import IORDF
from gu.plone.rdf.repositorymetadata import getContentUri
from zope.component import getUtility
from rdflib import URIRef
from zope.security import checkPermission


class AddItemView(BrowserView):

    def __call__(self):
        itemuuid = self.request.get('itemuuid')
        itembrain = uuidToCatalogBrain(itemuuid)
        mdgraph = IRepositoryMetadata(self.context)
        # FIXME: getContentUri should not be used here. (has side effects)
        subjecturi = getContentUri(itembrain.getObject())
        # FIXME: do another permission check here
        checkPermission('cmf.ModifyPortalContent', self.context)
        mdgraph.add((mdgraph.identifier, DC['hasPart'], URIRef(subjecturi)))
        handler = getUtility(IORDF).getHandler()
        handler.put(mdgraph)
        referer = self.request.get_header("referer")
        self.request.response.redirect(referer)
