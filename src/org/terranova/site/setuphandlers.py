import logging
logger = logging.getLogger(__name__)

from org.terranova.site import defaults


def setupVarious(context):
    logger.info('TerraNova site package setup handler')
    # only run for this product
    if context.readDataFile('org.terranova.site.marker.txt') is None:
        return

    portal = context.getSite()
    createRepositoryFolder(portal)

    # Create collection_details Collection (ArcheTypes)
    # XXX: Exporting/Importing this does not work.
    # XXX: Migrating to Dexterity Content although export seems to work, import fails.
    # XXX: See: https://bitbucket.org/ccaih/terranova/issue/11/move-collection-content-into-site-package
    id, title = "collection_details", "Collection Details"
    if id not in portal:
        portal.invokeFactory("Collection", id, title=title)
        query = [{
            "i": "Type",
            "o": "plone.app.querystring.operation.string.is",
            "v": "Repository Container"
        }]
        collection_details = portal["collection_details"]
        collection_details.setQuery(query)
        portal.portal_workflow.doActionFor(portal[id], "publish")


def _createFolder(context, folder_id, folder_title, workflow_state='publish', log_msg=None):
    # helper function for adding structural locations
    if not folder_id in context:
        if log_msg is None:
            log_msg = 'Creating container in %s for %s (%s)' % (context.title, folder_title, folder_id)
        logger.info(log_msg)
        context.invokeFactory('gu.repository.content.RepositoryContainer', folder_id, title=folder_title)
        context.portal_workflow.doActionFor(context[folder_id], workflow_state)


def _addRoleToPermission(context, role, permission, acquire=True):
    roles = [item['name'] for item in context.rolesOfPermission(permission) if item["selected"] == "SELECTED"]
    roles.append(role)
    context.manage_permission(permission, roles, acquire=acquire)


def createRepositoryFolder(site):
    # Add a root level container to hold repository items
    _createFolder(
        site,
        folder_id=defaults.REPOSITORY_FOLDER_ID,
        folder_title='Repository',
    )

    # Add "Member" role to the "gu.repository.content: Add Repository Item" permission
    _addRoleToPermission(site.repository, "Member", "gu.repository.content: Add Repository Item")
