
from dexterity.membrane.content.member import IMember

from plone.indexer import indexer
from plone.uuid.interfaces import IUUIDAware
from plone.indexer.interfaces import IIndexer

from Products.Archetypes.interfaces import IUIDCatalog

from zope.component import queryMultiAdapter


@indexer(IUUIDAware)
def contributorsIndexer(context):
    return context.Contributors


@indexer(IUUIDAware)
def friendlyCreatorIndexer(context):
    creator = context.Creator()
    properties = getUserProperties(context, creator)
    return properties.get("fullname", "") or creator


@indexer(IMember, IUIDCatalog)
def AT_UID_Title(object, **kw):
    # membrane objects have a default Title indexer registered,
    # let's reuse it to find
    # title value and convert value to str to satisfy stupid
    # ArcheTypes UID index

    title = ''
    indexer = queryMultiAdapter(
        (object, object.portal_catalog),
        IIndexer, name='Title'
    )

    if indexer is not None:
        title = indexer()

    if isinstance(title, unicode):
        return title.encode('utf-8')

    try:
        return str(title)
    except UnicodeDecodeError:
        object.getId()


def getUserProperties(context, userid):
    """Given a context and userid return a dict of user properties"""

    member = context.portal_membership.getMemberById(userid)

    properties = {}

    if member:
        for propertyId in context.portal_memberdata.propertyIds():
            properties[propertyId] = member.getProperty(propertyId)

    return properties
